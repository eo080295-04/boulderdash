Daily Scrum - Every Day at 9AM
==============================

1.What have I done/not done since last scrum?
2.What do I have to do Before next scrum?
3.What do I need help with? What do I need to find out?

2018/02/28 Jianqiu Chen

1.What has been done? 
JPanel for game setting, JPanel for rules
2.What do I have to do before next scrum?
>Create Game Board
>program the database so that user stats are showing properly
3. What do I need help with?
Create Game Board, need to figure out the programming side of game board.

2018/03/06 jianqiu Chen
1.What has been done?
link Game Panel to pop up panel 
2.what do I need to do before the next scrum?
make the game working in terms of character moving, 
all conditions and timer
3. what do I need help with?
timer